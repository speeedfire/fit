import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/StatContainer';
import './Stat.css';

const Stat: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Stat</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Stat</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Stat" />
      </IonContent>
    </IonPage>
  );
};

export default Stat;
