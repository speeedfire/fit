import React, { useState } from 'react';
import './ProfileContainer.css';
import { IonToggle, IonList, IonItem, IonIcon, IonLabel, IonInput,IonItemDivider,IonAvatar,IonCard, InputChangeEventDetail, CheckboxChangeEventDetail, IonCheckbox, IonSelectOption, IonSelect, SelectChangeEventDetail } from '@ionic/react';
import '../helpers/Storage';
import { getStorage, setStorage } from '../helpers/Storage';

interface ContainerProps {
  name: string;
}

const ProfileContainer: React.FC<ContainerProps> = ({ name }) => {
  const toggleDarkMode = () => {
    document.body.classList.toggle('dark');
  }

  const [userName, setUserName] = useState<string>('Demo');
  const [userGender, setUserGender] = useState<string>('male');
  const [userAge, setUserAge] = useState(31);
  const [userHeight, setUserHeight] = useState(180);
  const [userWeight, setUserWeight] = useState(70);
  const [userBodyFat, setUserBodyFat] = useState(0);

  /*
  getStorage('userName').then((data)=>{setUserName(data)});
  getStorage('userGender').then((data)=>{setUserGender(data)});
  getStorage('userAge').then((data)=>{setUserAge(data)});
  getStorage('userHeight').then((data)=>{setUserHeight(data)});
  getStorage('userWeight').then((data)=>{setUserWeight(data)});
  getStorage('userBodyFat').then((data)=>{setUserBodyFat(data)});
*/
  return (
    <div className="container">
      <strong>{name}</strong>
      <IonCard>
        <div className='header'>
        <IonAvatar>
            <img src="https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y" />
          </IonAvatar>
          <IonLabel>{userName}</IonLabel>
        </div>
      </IonCard>
      <IonList class="theme-list" lines="full">
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonLabel>
              Dark Mode
            </IonLabel>
            <IonToggle slot="end" onIonChange={toggleDarkMode}></IonToggle>
          </IonItem>

          <IonItemDivider>Name</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonInput value={userName} placeholder="Name" onIonChange={(e: CustomEvent<InputChangeEventDetail>) => {
               setUserName(e.detail.value!)
               setStorage('userName', e.detail.value!);
        }}></IonInput>
          </IonItem>

        <IonItemDivider>Gender</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonSelect value={userGender} placeholder="Select One" onIonChange={(e: CustomEvent<SelectChangeEventDetail>) => {
               setUserGender(e.detail.value!)
               setStorage('userGender', e.detail.value!);
        }}>
              <IonSelectOption value="female">Female</IonSelectOption>
              <IonSelectOption value="male">Male</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItemDivider>Age</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonInput value={userAge} placeholder="Age" onIonChange={(e: CustomEvent<InputChangeEventDetail>) => {
               setUserAge(parseInt(e.detail.value!))
               setStorage('userAge', e.detail.value!);
        }}></IonInput>
          </IonItem>

          <IonItemDivider>Height</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonInput value={userHeight} placeholder="Height" onIonChange={(e: CustomEvent<InputChangeEventDetail>) => {
               setUserHeight(parseInt(e.detail.value!))
               setStorage('userHeight', e.detail.value!);
        }}></IonInput>
          </IonItem>

          <IonItemDivider>Weight</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonInput value={userWeight} placeholder="Weight" onIonChange={(e: CustomEvent<InputChangeEventDetail>) => {
               setUserWeight(parseInt(e.detail.value!))
               setStorage('userWeight', e.detail.value!);
        }}></IonInput>
          </IonItem>

          <IonItemDivider>BodyFat</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <IonInput value={userBodyFat} placeholder="BodyFat" onIonChange={(e: CustomEvent<InputChangeEventDetail>) => {
               setUserBodyFat(parseInt(e.detail.value!))
               setStorage('userBodyFat', e.detail.value!);
        }}></IonInput>
          </IonItem>
        </IonList>
    </div>
  );
};

export default ProfileContainer;
