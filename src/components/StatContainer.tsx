import React, { useState } from 'react';
import './ProfileContainer.css';
import { IonToggle, IonList, IonItem, IonIcon, IonLabel, IonInput,IonItemDivider,IonAvatar,IonCard, InputChangeEventDetail, IonSelect, useIonViewDidEnter, IonSelectOption, SelectChangeEventDetail } from '@ionic/react';
import '../helpers/Storage';
import { getStorage, setStorage } from '../helpers/Storage';
import { exec } from 'child_process';

interface ContainerProps {
  name: string;
}

const ProfileContainer: React.FC<ContainerProps> = ({ name }) => {

  const [userGender, setUserGender] = useState<boolean>(false);
  const [userAge, setUserAge] = useState(31);
  const [userHeight, setUserHeight] = useState(180);
  const [userWeight, setUserWeight] = useState(70);
  const [userBodyFat, setUserBodyFat] = useState(0);

  const [userBMI, setUserBMI] = useState(0);
  const [userBMIDesc, setUserBMIDesc] = useState<String>('');
  const [userBMR, setUserBMR] = useState(0);
  const [userDailyCalorie, setUserDailyCalorie] = useState(0);
  const [userExercise, setUserExercise] = useState(1.2);

  const updateUserDailyCalorie = () => {
    setUserDailyCalorie(userBMR * userExercise);
  }

  useIonViewDidEnter(() => {
    getStorage('userGender').then((data)=>{setUserGender(data)});
    getStorage('userAge').then((data)=>{setUserAge(data)});
    getStorage('userHeight').then((data)=>{setUserHeight(data)});
    getStorage('userWeight').then((data)=>{setUserWeight(data)});
    getStorage('userBodyFat').then((data)=>{setUserBodyFat(data)});
    getStorage('userDailyCalorie').then((data)=>{setUserDailyCalorie(data)});

    let i = parseInt((userWeight/(userHeight*userHeight/10000)).toFixed(2));
    let j = 66.5 + ( 13.75 * userWeight ) + ( 5.003 * userHeight ) - ( 6.755 * userAge );

    setUserBMI(i);
    setUserBMR(j);

    if(userBMI < 18.6) {
      setUserBMIDesc('Under Weight: ');
    } else if(userBMI >= 18.6 && userBMI < 24.9) {
      setUserBMIDesc('Normal weight: ');
    } else {
      setUserBMIDesc('Over Weight :');
    }
    updateUserDailyCalorie();
  });

  return (
    <div className="container">
      <strong>{name}</strong>
      <IonList class="theme-list" lines="full">
          <IonItemDivider>BMI</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <strong>{userBMIDesc}{userBMI}</strong>
          </IonItem>

          <IonItemDivider>BMR</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <strong>{userBMR}</strong>
          </IonItem>

          <IonItem>
            <IonLabel>Exercise</IonLabel>
            <IonSelect value={userExercise} okText="Okay" cancelText="Dismiss" onIonChange={(e: CustomEvent<SelectChangeEventDetail>) => {
               setUserExercise(e.detail.value!)
               setStorage('setUserExercise', e.detail.value!);
               updateUserDailyCalorie();
            }}>
              <IonSelectOption value="1.2">Sedentary (little or no exercise)</IonSelectOption>
              <IonSelectOption value="1.375">Lightly active (light exercise/sports 1-3 days/week)</IonSelectOption>
              <IonSelectOption value="1.55">Moderately active (moderate exercise/sports 3-5 days/week)</IonSelectOption>
              <IonSelectOption value="1.725">Very active (hard exercise/sports 6-7 days a week)</IonSelectOption>
              <IonSelectOption value="1.9">If you are extra active (very hard exercise/sports & a physical job)</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItemDivider>Daily Calories</IonItemDivider>
          <IonItem>
            <IonIcon slot="start" icon="moon" class="component-icon component-icon-dark"></IonIcon>
            <strong>{userDailyCalorie}</strong>
          </IonItem>

        </IonList>
    </div>
  );
};

export default ProfileContainer;
