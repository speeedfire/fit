import { Storage } from '@capacitor/storage';

export async function setStorage(key: string, value: any): Promise<void> {
  await Storage.set({
    key: key,
    value: value
  });
}
export async function getStorage(key: string): Promise<any> {
  const item = await Storage.get({ key: key });
  return item.value;
}
export async function removeStorage(key: string): Promise<void> {
  await Storage.remove({
    key: key,
  });
}